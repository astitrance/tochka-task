## Тестовое задание для Точки от Сорокина Александра

## Инструкция

1. git clone https://gitlab.com/astitrance/tochka-task.git

2. docker-compose up -d

2.1. При сборке должны установиться зависимости из композера.
Если этого не произошло - выполняем: docker exec -it app sh -c "cd /var/www && composer install".

3. docker exec -it app sh -c "cd /var/www && cp ./.env.example ./.env && php artisan migrate && php artisan db:seed"

## Как пользоваться

1. Все товары: http://127.0.0.1:777/api/products
2. Конкретный: http://127.0.0.1:777/api/product/2
3. Запрос для добавления товара:
curl -X POST http://127.0.0.1:777/api/orders  -H "Accept: application/json"  -H "Content-Type: application/json"  -d '{"userId": "2", "details": {"email": "some@address.com"}, "products": [{"productId": 4, "productPriceId": 4, "amount": 3}, {"productId": 5, "productPriceId": 5, "amount": 2}]}'

4. Отчеты: прописать крон "* * * * * docker exec -it app sh -c "cd /var/www && php artisan schedule:run" >> /dev/null 2>&1"
4.1. Метод генерации отчетов протестировал, крон нет. Но он должен работать.

## Примечания

1. Письма смотрим в логе /storage/logs/laravel.log

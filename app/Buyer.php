<?php

namespace App;

class Buyer extends User
{
    private $email;

    protected $table = 'users';

    public function __get($key)
    {
        return $this->{$key} ?: parent::__get($key);
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }
}

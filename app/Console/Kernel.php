<?php

namespace App\Console;

use App\OrderNotification;
use App\OrderReport;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Еженедельный отчет по продажам
        $schedule->call(function () {
            // Даты
            $dateTo = date('Y-m-d H:i:s');
            $dateFrom = date('Y-m-d H:i:s', strtotime("{$dateTo} -1 day"));

            // Отчет
            /** @var OrderReport $orderReportEntity */
            $orderReportEntity = $this->app->make('App\OrderReport');
            $orderReport = $orderReportEntity->getSoldProductsReport($dateFrom, $dateTo);

            // Уведомитель
            /** @var OrderNotification $orderNotificationEntity */
            $orderNotificationEntity = $this->app->make('App\OrderNotification');

            $orderNotificationEntity->executeManagersDailyReportNotification($orderReport);
        })->dailyAt('09:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

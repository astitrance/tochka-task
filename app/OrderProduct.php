<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $table = 'orders_products';

    protected $fillable = ['product_id', 'order_id', 'product_price_id', 'amount'];

    public function OrderEntity()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }
}

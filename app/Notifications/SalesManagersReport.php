<?php

namespace App\Notifications;

use App\OrderReport;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SalesManagersReport extends Notification
{
    use Queueable;

    private $report;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(OrderReport $report)
    {
        $this->report = $report;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mailMessage = (new MailMessage)->greeting('Отчет по продажам');

        foreach ($this->report as $productReport) {
            $mailMessage->line("--------------------------------------");
            $mailMessage->line("Товар #{$productReport->id} {$productReport->name}");
            $mailMessage->line("Продано на сумму {$productReport->totalSum}");
            $mailMessage->line("Кол-во {$productReport->amount}");
            $mailMessage->line("--------------------------------------");
        }

        return $mailMessage;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

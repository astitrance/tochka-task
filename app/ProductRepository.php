<?php

namespace App;

class ProductRepository
{
    public static function getAll()
    {
        return Product::selectRaw('products.*, SUM(products_amount.amount) AS totalAmount')
            ->join('products_amount','products_amount.product_id', '=', 'products.id')
            ->leftJoin('orders_products', 'orders_products.product_id', '=', 'products.id')
            ->groupBy('products.id')
            ->havingRaw('SUM(products_amount.amount) > IFNULL(0, SUM(orders_products.amount))')
            ->with(['productAmountsEntity', 'productPriceEntity'])
            ->get();
    }
}

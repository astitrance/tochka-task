<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = ['name', 'description'];

    public function productPriceEntity()
    {
        return $this
            ->hasOne(ProductPrice::class, 'product_id')
            ->orderBy('created_at', 'DESC');
    }

    public function productAmountsEntity()
    {
        return $this->hasMany(ProductAmount::class, 'product_id');
    }

    public static function getAllProducts()
    {
        return ProductRepository::getAll();
    }
}

<?php

namespace App\Listeners;

use App\Events\NewOrder;
use App\OrderNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class SendNewOrderClientNotification
{
    private $notification;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(OrderNotification $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Handle the event.
     *
     * @param  NewOrder  $event
     * @return void
     */
    public function handle(NewOrder $event)
    {
        // Отправка уведомления клиенту
        $this->notification->executeClientNotification($event);
    }
}

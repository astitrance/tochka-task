<?php

namespace App\Listeners;

use App\Events\NewOrder;
use App\OrderNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendNewOrderManagersNotification
{
    private $notification;

    /**
     * Create the event listener.
     *
     * @param OrderNotification $notification
     */
    public function __construct(OrderNotification $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Handle the event.
     *
     * @param  NewOrder  $event
     * @return void
     */
    public function handle(NewOrder $event)
    {
        // Отправка уведомления менеджерам
        $this->notification->executeManagersNotification($event);
    }
}

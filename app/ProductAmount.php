<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductAmount extends Model
{
    protected $table = 'products_amount';

    protected $fillable = ['amount'];
}

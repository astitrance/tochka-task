<?php

namespace App\Http\Controllers;

use App\OrderProduct;
use App\Product;
use Illuminate\Support\Facades\Cache;

class ProductController extends Controller
{
    private $cacheSeconds = 60;

    public function index()
    {
        return Product::getAllProducts();
    }

    public function show($id)
    {
        // Выберем то, что можно кешировать
        $product = Cache::remember("product.{$id}", $this->cacheSeconds, function () use ($id) {
            return Product::find($id);
        });

        $productAsArray = array_merge(
            // То, что можно кешировать
            $product->toArray(),
            // То, что нельзя кешировать
            [
                'totalAmount' => $product->productAmountsEntity->sum('amount'),
                'productPriceEntity' => $product->productPriceEntity,
                'productAmountsEntity' => $product->productAmountsEntity,
            ]
        );

        return $productAsArray;
    }
}

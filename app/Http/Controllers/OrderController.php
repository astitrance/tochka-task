<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewOrderRequest;
use App\Order;
use App\Events\NewOrder;

class OrderController extends Controller
{
    public function store(NewOrderRequest $request)
    {
        try {
            $order = Order::createOrder($request->toArray());
            event(new NewOrder($order));
        } catch (\Exception $exception) {
            return [
                'status' => 0,
                'error' => $exception->getMessage()
            ];
        }

        return [
            'status' => 1,
            'order' => $order->id
        ];
    }
}

<?php

namespace App;

use App\Events\NewOrder;
use App\Notifications\NewOrderManagersNotification;
use App\Notifications\SalesManagersReport;
use Illuminate\Support\Facades\Notification;
use App\Notifications\NewOrderClientNotification;

class OrderNotification
{
    public function executeClientNotification(NewOrder $event)
    {
        $order = $event->getOrder();
        $buyer = clone $order->userEntity;

        // Если в заказе указан email - отправим на него письмо
        if ($orderEmail = json_decode($order->details_json)->email ?? null) {
            $buyer->setEmail($orderEmail);
        }

        Notification::send($buyer, new NewOrderClientNotification($order));
    }

    public function executeManagersNotification(NewOrder $event)
    {
        $order = $event->getOrder();

        $managers = User::whereIn('id', config('app.managerIds'))->get();

        Notification::send($managers, new NewOrderManagersNotification($order));
    }

    public function executeManagersDailyReportNotification(OrderReport $report)
    {
        $managers = User::whereIn('id', config('app.managerIds'))->get();

        Notification::send($managers, new SalesManagersReport($report));
    }
}

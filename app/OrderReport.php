<?php

namespace App;

class OrderReport
{
    /**
     * Кол-во проданных единиц товара и его суммарная прибыль за сутки
     *
     * @param string $dateFrom
     * @param string $dateTo
     * @return
     */
    public function getSoldProductsReport($dateFrom, $dateTo)
    {
        return OrderProduct::selectRaw(
<<<'SQL_AS_TEXT'
products.id as id,
products.name as name,
SUM(orders_products.amount) as amount,
SUM(products_price.price) as totalSum
SQL_AS_TEXT
            )
            ->join('products', 'products.id', '=', 'orders_products.product_id')
            ->join('products_price', 'products_price.id', '=', 'orders_products.product_price_id')
            ->whereBetween('orders_products.created_at', [$dateFrom, $dateTo])
            ->groupBy('products.id')
            ->get();
    }
}

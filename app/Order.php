<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;

/** @property string $details_json */

class Order extends Model
{
    use Notifiable;

    protected $table = 'orders';

    protected $fillable = ['user_id', 'details_json'];

    public function OrderProductsEntity()
    {
        return $this
            ->hasMany(OrderProduct::class, 'order_id');
    }

    public function userEntity()
    {
        return $this->belongsTo(Buyer::class, 'user_id', 'id');
    }

    public static function createOrder(array $data)
    {
        return DB::transaction(function () use ($data) {
            $order = self::create([
                // Пользователь с индексом 1 - для неавторизованных заказов
                // Не хочу делать в базе поле nullable
                'user_id' => $data['userId'] ?? 1,
                'details_json' => isset($data['details']) ? json_encode($data['details']) : '{}',
            ]);

            if ($order->id) {
                foreach ($data['products'] as $product) {

                    $productEntity = Product::with(['productAmountsEntity', 'productPriceEntity'])
                        ->find($product['productId']);

                    // Проверка кол-ва товаров
                    $productAmountStore = $productEntity->productAmountsEntity->sum('amount');
                    $productAmountSold = OrderProduct::where('product_id', $product['productId'])->sum('amount');

                    // Не продаем больше, чем имеем
                    if (($productAmountStore - $productAmountSold - $product['amount']) < 0) {
                        throw new \Exception("Out of stock product #{$product['productId']}");
                    }

                    OrderProduct::create([
                        'order_id' => $order->id,
                        'product_id' => $product['productId'],
                        'product_price_id' => $productEntity->productPriceEntity->id,
                        'amount' => $product['amount'],
                    ]);
                }
            } else {
                throw new \Exception('Can\'t create order');
            }

            return $order;
        });
    }
}

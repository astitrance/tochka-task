<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userIds = [];
        $chunkCount = 10;

        DB::table('users')->orderBy('id')->chunk($chunkCount, function ($users) use (&$userIds) {
            foreach ($users as $user) {
                $userIds[] = $user->id;
            }
        });

        for ($i = 1; $i <= $chunkCount; $i++) {
            DB::table('orders')->insert([
                'user_id' => $userIds[array_rand($userIds)],
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'details_json' => '{}',
            ]);
        }
    }
}
